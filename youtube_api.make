; Make file for Youtube API module.
; Fetch the Google Youtube library from Github.

api = 2
core = 7.x

libraries[google-api-php-client][download][type] = get
libraries[google-api-php-client][download][url] = https://github.com/googleapis/google-api-php-client/archive/1.1.4.tar.gz
libraries[google-api-php-client][destination] = libraries
